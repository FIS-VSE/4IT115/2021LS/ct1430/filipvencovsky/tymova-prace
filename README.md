# Týmová práce

Tento projekt obsahuje informace o týmové práci.

Jak postupovat?

1. Vyberte si **téma** a vytvořte týmy (3-5 členů).
1. **Rozdělte si odpovědnosti.** Je povinností všech se podílet na všech aktivitách. Hodí se ale, aby za každou oblast byl odpovědný primárně jeden člověk, který může ostatním zadávat úkoly. Oblasti jsou minimálně tyto:
   - **Řízení projektu**, zahrnuje řízení diskuze o metodě vývoje, metodě zajištění kvality, nastavení workflow, diskuzi k tvorbě issues a jejich zařazení a ohodnocení, nastavení boardu, dodržení termínu, splění zadání, rozdělení na doručitelné iterace
   - **User Experience**, zahrnuje řízení diskuze o účelu a funkcionalitě aplikace, cílové skupině, odpovědnost za zahrnutí uživatelů do návrhu a ověřování aplikace, výběru UX metod
   - **Kód**, architektura aplikace, použití návrhových vzorů, pokrytí jednotkovými testy, srozumitelnost kódu, použití komentářů a vývojářské dokumentace
   - **Správa gitlabu**, založení projektu, přidělení práv, nastavení a dodržování pravidel práce s větvemi a správa verzí, správa pipeline
   - **Správa projektu a sestavení**, správnost Maven konfigurace a skriptu pro stestavení, funkčnost výsledného produktu
1. Pro týmovou práci **založte** nový projekt.
1. **Pozvěte** všechny členy týmu.
1. Založte **wiki** na výchozí "home" stránce podle [vzoru](https://gitlab.com/FIS-VSE/4IT115/2023LS/ut1430/cviceni/tymova-prace/-/wikis/home).
1. Vymyslete **účel aplikace** jako krátkou anotaci.
1. Definujte **funkcionalitu aplikace** ve formátu *user stories*.
1. Na základě diskuze **metod vývoje** určete, jak budete postupovat a připravte pro to podmínky v gitlabu.
1. Držte se Vámi určeného postupu a **navrhněte aplikaci**. Je třeba dodržet [společné zadání](https://java.vse.cz/4it115/ZadaniDruheUlohy).
1. Proveďte **inspekci návrhu** jinému týmu.
1. **Obhajte návrh** v čase výuky [v předem určeném termínu](https://java.vse.cz/4it115/Vencovsky).
1. Držte se Vámi určeného postupu a **naimplementujte aplikaci** podle návrhu. Je třeba dodržet [společné zadání](https://java.vse.cz/4it115/ZadaniDruheUlohy).
1. **Obhajte aplikaci** ve zkouškovém období v termínu, na který se přihlásíte.
